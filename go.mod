module bitbucket.org/monahawk/microservices-go

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/jinzhu/gorm v1.9.16
	github.com/stretchr/testify v1.6.1
	google.golang.org/grpc v1.31.0
	google.golang.org/protobuf v1.25.0
)

replace github.com/coreos/go-systemd => github.com/coreos/go-systemd/v22 v22.1.0
