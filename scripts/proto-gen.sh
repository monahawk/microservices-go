#!/bin/bash

set -e

while getopts ":s:f:" opt; do
    case $opt in
        s) SERVICE="$OPTARG"
        ;;
        f) FOLDER="$OPTARG"
        ;;
        \?) echo "Invalid option -$OPTARG" >&2
        ;;
    esac
done

function generate {
	protoc \
    --proto_path=proto \
    proto/$1.proto \
    --go_out=plugins=grpc:$GOPATH/src
}

  echo "Generating Protocol buffer for $SERVICE"
  generate $SERVICE $FOLDER

