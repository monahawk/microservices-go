package history_test

import (
	"context"
	"testing"

	history "bitbucket.org/monahawk/microservices-go/internal/history/grpc"
	"bitbucket.org/monahawk/microservices-go/pkg/pb/commons"
	pb "bitbucket.org/monahawk/microservices-go/pkg/pb/history"
	"github.com/stretchr/testify/require"
)

func TestServerCreateSerachHistory(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name          string
		searchHistory *pb.SearchHistoryEntry
		code          commons.Status_StatusCode
	}{
		{
			name:          "success",
			searchHistory: GenerateSearchHistory(),
			code:          commons.Status_OK,
		},
	}

	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			server := history.NewSearchHistoryServer()
			res, err := server.Create(context.Background(), tc.searchHistory)
			require.NoError(t, err)
			require.NotNil(t, res)
		})
	}
}
