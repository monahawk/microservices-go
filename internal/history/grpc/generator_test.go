package history_test

import (
	pb "bitbucket.org/monahawk/microservices-go/pkg/pb/history"
)

func GenerateSearchHistory() *pb.SearchHistoryEntry {
	return &pb.SearchHistoryEntry{
		Id:      0,
		Date:    0,
		Keyword: "random",
		UserId:  1,
		Weight:  0,
	}
}
