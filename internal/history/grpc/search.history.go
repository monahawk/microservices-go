package history

import (
	"context"
	"fmt"
	"log"
	"time"

	repo "bitbucket.org/monahawk/microservices-go/internal/history/repo"
	"bitbucket.org/monahawk/microservices-go/pkg/pb/commons"
	pb "bitbucket.org/monahawk/microservices-go/pkg/pb/history"
)

type SearchHistoryServer struct {
}

func NewSearchHistoryServer() *SearchHistoryServer {
	return &SearchHistoryServer{}
}

// Create bla
func (server *SearchHistoryServer) Create(
	ctx context.Context,
	req *pb.SearchHistoryEntry,
) (*commons.Status, error) {
	log.Printf("received the following request: %v", req)

	sH := &repo.SearchHistory{}
	sH.Keyword = req.Keyword
	weight := req.Weight
	if weight <= 0 {
		weight = 1
	}
	sH.Weight = weight
	date := req.Date
	if date <= 0 {
		date = time.Now().Unix()
	}
	sH.Date = date

	err := sH.Create()
	if err != nil {
		return &commons.Status{
			Code: commons.Status_DATA_LOSS,
			Msg:  fmt.Sprintln(err),
		}, err
	}
	return &commons.Status{
		Code: commons.Status_OK,
		Msg:  "",
	}, nil
}
