package history

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// SearchHistory for storing search histories
type SearchHistory struct {
	gorm.Model
	User    User
	UserID  int `gorm:"ForeignKey:UserID"`
	Date    int64
	Keyword string
	Weight  int32
}

// User for some minor info
type User struct {
	gorm.Model
	UserID        int64
	SearchHistory []SearchHistory
}

func (s *SearchHistory) Create() error {
	db, err := gorm.Open("mysql", fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", "go", "go", "history"))
	if err != nil {
		return err
	}

	db.AutoMigrate(&SearchHistory{}, &User{})
	db.Create(s)
	return nil
}

func (s *SearchHistory) Update(updated *SearchHistory) error {
	db, err := gorm.Open("mysql", "go:go@/history?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		return err
	}
	db.Model(s).Update(updated)

	return nil
}

func (s *SearchHistory) Delete() error {
	db, err := gorm.Open("mysql", fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", "go", "go", "history"))
	if err != nil {
		return err
	}
	db.Delete(s)
	return nil
}
