
PROJECTNAME=$(shell basename "$(PWD)")


BUILDPATH=$(CURDIR)
GO=$(shell which go)
GOINSTALL=$(GO) install
GOCLEAN=$(GO) clean
GOFILES=$(wildcard *.go)

EXENAME=main

## gen_history: Generate history Service's proto files
gen_history:
	scripts/proto-gen.sh -s history -f internal
## gen_indexer: Generate indexer Service's proto files
gen_indexer:
	scripts/proto-gen.sh -s indexer -f internal
## gen_search: Generate search Service's proto files
gen_search:
	scripts/proto-gen.sh -s search -f internal
gen_commons:
	scripts/proto-gen.sh -s commons -f pkg
gen_preferences:
	scripts/proto-gen.sh -s preferences -f internal
gen_recommender:
	scripts/proto-gen.sh -s recommender -f internal
gen_collections:
	scripts/proto-gen.sh -s collections -f internal
## gen: Generate all the proto definitions
gen: gen_collections gen_recommender gen_preferences gen_commons gen_search gen_indexer gen_history
	@echo Generated all proto definitions under pkg/pb
	@tree pkg/pb

clean:
	rm -r pkg/pb

test: 
	go test -cover -race ./...

.PHONY: help
all: help
help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo
